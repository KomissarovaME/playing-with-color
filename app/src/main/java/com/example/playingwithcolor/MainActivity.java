package com.example.playingwithcolor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private ConstraintLayout mConstraintLayout;

    protected void onCreate(Bundle savedInstanceState, ConstraintLayout mConstraintLayout) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mConstraintLayout =(ConstraintLayout) findViewById(R.id.constraintlayout);
    }

    public void onThighFrightenedNymphButtonClick (View view){
        mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.Thigh_frightened_nymph));
    }

    public void onCardinalOnStrawButtonClick(View view) {
        mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.Cardinal_on_straw));
    }

    public void onNavarinoFlameAndSmokeButtonClick (View view) {
        mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.Navarino_flame_and_smoke));
    }
}
